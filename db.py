"""
Database schema and connection pool.
"""
from sqlalchemy import create_engine, Column, Integer, String, Enum, \
  ForeignKey, PickleType
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship, backref

engine = create_engine("sqlite:///database.db", echo=True)
Session = sessionmaker(bind=engine)
Base = declarative_base()

# make objects json friendly
def todict(self):
  ret = {}
  for col in self.__table__.columns:
    yield(col.name, getattr(self, col.name))

def todictiter(self):
  return self.todict()

Base.todict = todict
Base.__iter__ = todictiter

class Server(Base):
  __tablename__ = "server"
  id = Column(Integer, primary_key=True)
  name = Column(String)
  address = Column(String)
  user = Column(String)
  directory = Column(String)
  # instances

class Service(Base):
  __tablename__ = "service"
  id = Column(Integer, primary_key=True)
  name = Column(String)
  source = Column(String)
  # instances

class Instance(Base):
  __tablename__ = "instance"
  id = Column(Integer, primary_key=True)
  service_id = Column(Integer, ForeignKey("service.id"))
  service = relationship("Service", backref="instances", order_by="Service.id")
  server_id = Column(Integer, ForeignKey("server.id"))
  server = relationship("Server", backref="instances", order_by="Service.id")
  ref = Column(String)
  downloaded_sha1 = Column(String)
  installed_sha1 = Column(String)
  running_sha1 = Column(String)
  name = Column(String)
  config = Column(String)

def create_tables():
  Base.metadata.create_all(bind=engine)

def create_session():
  return Session()

if __name__ == "__main__":
  create_tables()