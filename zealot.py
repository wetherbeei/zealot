from __future__ import division, absolute_import, print_function
from flask import Flask, render_template, request, make_response, redirect, \
  abort, url_for, session, jsonify, flash, Response
import os.path
from functools import wraps
from StringIO import StringIO
from fabric import api
from fabric.contrib import files
from fabric.exceptions import NetworkError
import yaml
import re

# app imports
import config
import db

app = Flask(__name__)
app.debug = True
app.secret_key = config.get("secretkey")

api.env.sudo_prompt = "assword:" # Sudo password: OR Password:
api.env.always_use_pty = True

def check_auth(u, p):
  return config.get("username") == u and config.get("password") == p

def authenticate():
  return Response('Could not verify your access level for that URL.\n'
  'You have to login with proper credentials', 401,
  {'WWW-Authenticate': 'Basic realm="Login Required"'})

def auth(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    auth = request.authorization
    if not auth or not check_auth(auth.username, auth.password):
      return authenticate()
    print(auth)
    return f(*args, **kwargs)
  return decorated

# POST = create, PUT = update, DELETE = delete

@app.route("/")
@auth
def index():
  return redirect(url_for("server"))

@app.route("/server", methods=["GET", "POST"])
@auth
def server():
  dbs = db.create_session()
  if request.method == "POST":
    name = request.form.get("name", None)
    address = request.form.get("address", None)
    user = request.form.get("user", None)
    password = request.form.get("password", None)
    directory = request.form.get("directory", None)
    if not directory.endswith("/"):
      directory += "/"
    key_file = os.path.expanduser(config.get("keyfilepub"))
    public_key = open(key_file).read()
    with api.settings(host_string=address, user=user, password=password, warn_only=True):
      api.run("mkdir ~/.ssh")
      api.run("touch ~/.ssh/authorized_keys")
      files.append("~/.ssh/authorized_keys", public_key)
      api.run("mkdir -p '%s'" % directory)
    server = db.Server(name=name, address=address, user=user, 
      directory=directory)
    dbs.add(server)
    dbs.commit()
    flash("Created %s/%s" % (server.address, server.directory), "success")
  servers = dbs.query(db.Server).all()
  for s in servers:
    with ServerContext(s):
      s.s_mem = get_server_memory()
      s.s_cpu = get_server_cpu()
  return render_template("servers.html", servers=servers)

@app.route("/server/<int:id>", methods=["GET"])
@auth
def server_id(id):
  dbs = db.create_session()
  server = dbs.query(db.Server).filter_by(id=id).first()
  if not server:
    abort(404, "Server not found")
  with ServerContext(server):
    server.top = api.run("top -b -n 1")
    server.free = api.run("free -m")
    server.public_ssh = get_file("~/.ssh/id_rsa.pub")

  return render_template("server.html", server=server)

@app.route("/service", methods=["GET", "POST"])
@auth
def service():
  session = db.create_session()
  if request.method == "POST":
    name = request.form.get("name", None)
    source = request.form.get("source", None)
    service = db.Service(name=name, source=source)
    session.add(service)
    session.commit()
  services = session.query(db.Service).all()
  return render_template("services.html", services=services)

@app.route("/service/<int:id>")
@auth
def service_id(id):
  session = db.create_session()
  service = session.query(db.Service).filter_by(id=id).first()
  if not service:
    return abort(404, "Service not found")
  refs = get_refs(service.source)
  
  servers = session.query(db.Server).all()
  return render_template("service.html", servers=servers, refs=refs,
    service=service)

@app.route("/instance", methods=["POST"])
@auth
def instance():
  """
  Instance creation:
  - Select target server
  - Select target ref (git ls-remote)
  """
  session = db.create_session()
  server_id = request.form.get("server_id", None)
  print(server_id)
  server = session.query(db.Server).filter_by(id=server_id).first()
  if not server:
    return abort(404, "Server not found")
  service_id = request.form.get("service_id", None)
  service = session.query(db.Service).filter_by(id=service_id).first()
  if not service:
    return abort(404, "Service not found")
  name = request.form.get("name", None)
  ref = request.form.get("ref", None)
  instance = db.Instance(service_id=service.id, server_id=server.id, ref=ref, 
    name=name, config="")
  session.add(instance)
  session.commit()
  with ServerContext(server):
    path = get_instance_path(instance)
    api.run("rm -rf '%s" % path)
    api.run("mkdir '%s'" % path)
    with api.cd(path):
      api.run("git clone %s . -b %s" % (service.source, instance.ref))
      cfg = yaml.load(get_file("config.yaml"))
      if not "install" in cfg or not "run" in cfg:
        flash("config.yaml needs install and run keys", "error")
        return redirect(url_for("instance_id_delete", instance.id))
      flash("Created new instance of %s on %s" % (service.name, server.name), 
        "success")
      instance.downloaded_sha1 = api.run("git rev-parse HEAD")
      session.commit()
  if server.down:
    flash("Server is down, cannot create instance", "error")
    return redirect(url_for("service_id", id=service_id))
  return redirect(url_for("instance_id", id=instance.id))

@app.route("/instance/<int:id>", methods=["GET", "PUT", "POST"])
@auth
def instance_id(id):
  session = db.create_session()
  instance = session.query(db.Instance).filter_by(id=id).first()
  if not instance:
    return abort(404, "Instance not found")
  if request.form.get("delete", False):
    return redirect(url_for("instance_id_delete", id=id))

  with InstanceContext(instance):
    pid = get_pid()
    config_sha1 = api.run("git rev-parse %s" % instance.ref)
    cfg = yaml.load(get_file("config.yaml"))
    if not cfg:
      flash("config.yaml not found", "error")
      return redirect(url_for("instance_id_delete", id=id))
  if instance.server.down:
    return render_template("instance_down.html", instance=instance)

  if request.method == "POST":
    with InstanceContext(instance):
      if request.form.get("configure", False):
        if "files" in cfg:
          for key, default_value in cfg["files"].iteritems():
            form_value = request.form.get("file_" + key, None)
            # from file on server
            loc = default_value
            put_file(loc, form_value.replace("\r\n", "\n"))
        api.run("git commit -am 'Zealot: config file updates'")
        instance.config = request.form.get("config", "")
        config_sha1 = api.run("git rev-parse %s" % instance.ref)
        session.commit()
      elif request.form.get("pull", False):
        fetch_result = api.run("git fetch")
        merge_result = api.run("git merge origin/%s" % instance.ref)
        if "CONFLICT" in merge_result:
          # allow committing of merge markers
          api.run("git reset --mixed")
        instance.downloaded_sha1 = api.run("git rev-parse origin/%s" % instance.ref)
        session.commit()
        cfg = yaml.load(get_file("config.yaml"))
        flash(fetch_result, "info")
        flash(merge_result, "info")
      elif request.form.get("install", False):
        result = api.run(cfg["install"])
        if result.succeeded:
          instance.installed_sha1 = instance.downloaded_sha1
          flash(result, "success")
        else:
          instance.installed_sha1 = None
          flash(result, "error")
        session.commit()
      elif request.form.get("start", False):
        runline = "%s %s" % (cfg["run"], instance.config)
        api.run("(%s >& run.log < /dev/null & echo $! > ./pid.lock && disown -h && sleep 5)" 
          % runline, pty=True)
        pid = get_pid()
        instance.running_sha1 = "%s-%s" % (instance.installed_sha1, config_sha1)
        session.commit()
        flash("Started process", "success")
      elif request.form.get("stop", False):
        result = api.run("kill %s" % pid)
        if result.succeeded:
          flash("Killed process", "success")
        else:
          flash("Process already dead", "info")
      elif request.form.get("kill", False):
        result = api.run("kill -9 %s" % pid)
        if result.succeeded:
          flash("Force killed process", "success")
        else:
          flash("Process already dead", "info")
      return redirect(url_for("instance_id", id=id))
  with InstanceContext(instance):
    if pid > 0:
      status = api.run("pstree -a -l -A %s" % pid)
    if pid <= 0 or status.failed or len(status) == 0:
      status = "Not running"
    file_values = []
    # fill in config
    if "files" in cfg:
      for key, default_value in cfg["files"].iteritems():
        # from file on server
        loc = default_value
        val = get_file(loc)
        t = "file"
        help = "File: %s" % loc
        file_values.append({"key": key, "value": val, "help": help})

  refs = get_refs(instance.service.source, instance.ref)
  return render_template("instance.html", instance=instance, run_cmd=cfg["run"],
    remote_sha1=refs[instance.ref], status=status, config_files=file_values,
    config_sha1=config_sha1)

@app.route("/instance/<int:id>/log", methods=["GET"])
@auth
def instance_id_log(id):
  session = db.create_session()
  instance = session.query(db.Instance).filter_by(id=id).first()
  with InstanceContext(instance):
    log = get_file("run.log")
  return render_template("instance_log.html", instance=instance, log=log)

@app.route("/instance/<int:id>/delete", methods=["GET", "POST"])
@auth
def instance_id_delete(id):
  session = db.create_session()
  instance = session.query(db.Instance).filter_by(id=id).first()
  if request.method == "POST":
    with InstanceContext(instance):
      pid = get_pid()
      api.run("kill %s" % pid)
    with ServerContext(instance.server):
      path = get_instance_path(instance)
      api.run("rm -rf '%s'" % path)
    session.delete(instance)
    flash("Deleted instance %s" % instance.name, "success")
    session.commit()
    return redirect(url_for("server"))
  return render_template("instance_delete.html", instance=instance)

def get_refs(uri, ref=None):
  if ref:
    cmd = "git ls-remote -h -t '%s' %s" % (uri, ref)
  else:
    cmd = "git ls-remote -h -t '%s'" % uri
  refs = api.local(cmd, capture=True)
  refs = [s.split("\t", 2) for s in refs.split("\n")]
  ret = {}
  for sha1, name in refs:
    if name.startswith("refs/heads/"):
      name = name[11:]
    else:
      name = name[5:]
    ret.update({name: sha1})
  return ret

def get_instance_path(instance):
  return os.path.join(instance.server.directory, str(instance.id))

def get_pid():
  """Must be called inside a valid InstanceContext"""
  pid = api.run("cat pid.lock")
  if pid.failed:
    return -1
  return int(pid)

def get_file(name):
  """Must be called inside a valid InstanceContext"""
  config_data = StringIO()
  api.get(name, config_data)
  config_data.seek(0)
  return config_data.read()

def put_file(loc, data):
  """Must be called inside a valid InstanceContext"""
  config_data = StringIO(data)
  api.put(config_data, loc)

def get_server_memory():
  """Must be called inside a valid ServerContext"""
  data = api.run("free -m")
  lines = data.split("\n")
  total = int(re.findall(r"[0-9]+", lines[1].split(":")[1])[0])
  used = int(re.findall(r"[0-9]+", lines[2].split(":")[1])[0])
  return {"total": total, "used": used, "percent": (used/total)*100}

def get_server_cpu():
  cpus = int(api.run("cat /proc/cpuinfo | grep processor | wc -l"))
  load = map(lambda x: float(x)*100, re.findall(r"[0-9\.]+", api.run("uptime"))[-3:])
  return {"one": load[0]/cpus, "five": load[1]/cpus, "fifteen": load[2]/cpus}

class Context:
  def __init__(self, server, directory):
    self.server = server
    self.directory = directory

  def __enter__(self):
    self.context = api.settings(host_string=self.server.address, 
      user=self.server.user, warn_only=True, pty=True)
    self.cd = api.cd(self.directory)
    api.env.password = request.form.get("sudo_password", "")
    self.context.__enter__()
    self.cd.__enter__()

  def __exit__(self, t, value, tb):
    api.env.password = ""
    self.cd.__exit__(t, value, tb)
    self.context.__exit__(t, value, tb)
    if not t:
      self.server.down = False
    if isinstance(value, NetworkError):
      self.server.down = value
      return True

class ServerContext(Context):
  def __init__(self, server):
    Context.__init__(self, server, server.directory)

class InstanceContext(Context):
  def __init__(self, instance):
    Context.__init__(self, instance.server, get_instance_path(instance))

if __name__ == "__main__":
  db.create_tables()
  from gevent.wsgi import WSGIServer
  s = WSGIServer(("", config.get("port")), app)
  s.serve_forever()
