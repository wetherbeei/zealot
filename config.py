
config = {
  "port": 8100,
  "keyfile": "~/.ssh/id_rsa",
  "keyfilepub": "~/.ssh/id_rsa.pub",
  "secretkey": "\xbb`\xf7a\xde!\xe4\x9e\xf5Gx\xaf'\x93\x88<z\xb0z\x16\xf6\xa7O9",
  "username": "admin",
  "password": ""
}

def get(key):
  return config.get(key, None)

import argparse
parser = argparse.ArgumentParser(description="Zealot")
for (key, val) in config.iteritems():
  parser.add_argument("--" + key, type=type(val))
args = vars(parser.parse_args())
for (key, val) in args.iteritems():
  if val:
    config[key] = val
print(config)